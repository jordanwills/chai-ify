# README #

### What is this repository for? ###

This is simply a Chai-based assertions library. Hopefully it can prevent having to search arround for the correct syntax and chains to use and will speed up basic element assertions

This package is completely open-source and can be attributed to through a PR. This repository is also fork-able(?), so feel free to steal a bit of code, on the house.
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Install Node.js
* In your terminal, use the command 'npm i chai-ify'
* Require the library, i.e. 'import { verifyElement } from 'chai-ify';'
* Use commands within your code!

For example:

> ci.verifyElement({
>
>   > element: cy.get('#header'),
>
>   > visible: true,
>
>   > text: 'This is the header!',
>
> }),

This will assert that the the *element* supplied is visible, and has the text 'This is the header!'

### Who do I talk to? ###

* For help or questions, please contact [jordanewills@gmail.com](jordanewills@gmail.com)