declare namespace Chaiify {
  interface ChaiVerify {
    /**
     * # Element Verification
     * @param {*} parameters A JS object containing _{ "key": "value" }_ pairs to be verified
     * @example
     * ```js
     * const ci = require("chai-ify")
     * ci.verifyElement({
     *      element: "container > div > button",
     *      visible: true, // It should be visible
     *      "color": #ff0000 // It should be red
     *      "attribute: type": "button" // It should have an attribute called "type" with a value of "button"
     * })
     * ```
     * @note It is not a requirement to encapsulate _certain_ keys and/or values with quotation marks
     * @note Attributes __must__ be prefixed with _"attribute: ..."_, as can be seen in the __@example__
     */
    verifyElement(parameters: object): void;
    /**
     * # Element Verification
     * @param {*} parameters A JS object containing _{ "key": "value" }_ pairs to be verified
     * @example
     * ```js
     * const ci = require("chai-ify")
     * ci.verifyElement({
     *      element: "container > div > button",
     *      visible: true, // It should be visible
     *      "color": #ff0000 // It should be red
     *      "attribute: type": "button" // It should have an attribute called "type" with a value of "button"
     * })
     * ```
     * @note It is not a requirement to encapsulate _certain_ keys and/or values with quotation marks
     * @note Attributes __must__ be prefixed with _"attribute: ..."_, as can be seen in the __@example__
     */
    verifyElement<T>(parameters?: T): void;
  }
}

declare const chaiify: Chaiify.ChaiVerify;

declare module "chai-ify" {
  export = chaiify;
}
