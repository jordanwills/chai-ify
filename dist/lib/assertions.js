//      ▄      ▄
//     ▐▒▀▄▄▄▄▀▒▌
//    ▄▀▒▒▒▒▒▒▒▒▓▀▄
//   ▄▀░█░░░░█░░▒▒▒▐
//   ▌░░░░░░░░░░░▒▒▐  Chai assertions made easy(er)!
//  ▐▒░██▒▒░░░░░░░▒▐  (Stolen from https://dogemuchwow.com/ascii-art-doge/)
//  ▐▒░▓▓▒▒▒░░░░░░▄▀
//   ▀▄░▀▀▀▀░░░░▄▀
//     ▀▀▄▄▄▄▄▀▀

const units = require("web-units");
import { _cssTypes, _boolTypes, _cssUnitsLength } from "./types";

/**
 * # Element Verification
 * @param {*} parameters A JS object containing _{ "key": "value" }_ pairs to be verified
 * @example
 * ```js
 * verifyElement({
 *      element: "container > div > button",
 *      visible: true, // It should be visible
 *      "color": #ff0000 // It should be red
 *      "attribute: type": "button" // It should have an attribute called "type" with a value of "button"
 * })
 * ```
 * @note It is not a requirement to encapsulate _certain_ keys and/or values with quotation marks
 * @note Attributes __must__ be prefixed with _"attribute: ..."_, as can be seen in the __@example__
 */
function verifyElement(parameters = {}) {
  let size = Object.size(parameters);

  const tryFn = (fn, fallback = null) => {
    try {
      return fn()
    } catch(e) {
      return fallback;
    }
  }

  let element = tryFn(() => {
    if (typeof parameters["element"] === 'string') {
      return cy.get(parameters["element"]);
    } else {
      return parameters["element"];
    }
  })

  for (let i = 0; i < size; i++) {
    if (Object.keys(parameters)[i]) {
      let func = _determineType(Object.keys(parameters)[i]);

      element.should(($el) => {
        if (Object.keys(parameters)[i] !== "element") {
          func(
            $el,
            String(Object.keys(parameters)[i]),
            _convertUnits($el, String(Object.values(parameters)[i]))
          );
        }
      });
    }
  }
}

/**
 * @param {*} key The relative given object key
 * @returns The function corresponding to that keys expected return type
 */
function _determineType(key) {
  if (key !== "element") {
    if (key.toLowerCase().slice(0, 10) === "attribute:") {
      return _verifyAttribute;
    } else if (arrContains(String(key), _cssTypes)) {
      return _verifyCss;
    } else if (arrContains(String(key), _boolTypes)) {
      return _verifyBool;
    } else {
      return _verifyGeneric;
    }
  }
}

/**
 * @param {*} el The returned element of a cy.get() method
 * @param {*} key The relative given object key with a _"attribute:"_ prefix
 * @param {*} value The relative given object value
 * @returns A Cypress assertion based on the keys relationship with the element vs the expected value, specifically for DOM attribute assertions
 */
function _verifyAttribute(el, key, value) {
  expect(el).to.have.attr(_formatAttribute(key), value);
}

/**
 * @param {*} str The whole key for an attribute prefixed with _"attribute:"_
 * @returns A string with the _"attribute:"_ prefix removed, and any excess whitespace removed from the start
 */
function _formatAttribute(str) {
  if (str.toLowerCase().slice(0, 10) === "attribute:") {
    str = str.slice(10).trim();
  }
  return str;
}

/**
 * @param {*} el The returned element of a cy.get() method
 * @param {*} key The relative given object key
 * @param {*} value The relative given object value
 * @returns A Cypress assertion based on the keys relationship with the element vs the expected value, specifically for CSS assertions
 */
function _verifyCss(el, key, value) {
  expect(el).to.have.css(key, value);
}

/**
 * @param {*} el The returned element of a cy.get() method
 * @param {*} key The relative given object key
 * @param {*} value The relative given object value
 * @returns A Cypress assertion based on the keys relationship with the element vs the expected value, specifically for boolean assertions
 */
function _verifyBool(el, key, value) {
  if (value === true || value === "true") {
    eval(`expect(el).to.${key}`);
  } else if (value === false || value === "false") {
    eval(`expect(el).to.not.${key}`);
  } else {
    console.log(`No value provided for ${key}, defaulting value to true`);
    eval(`expect(el).to.${key}`);
  }
}

/**
 * @param {*} el The returned element of a cy.get() method
 * @param {*} key The relative given object key
 * @param {*} value The relative given object value
 * @returns A Cypress assertion based on the keys relationship with the element vs the expected value
 */
function _verifyGeneric(el, key, value) {
  eval(`expect(el).to.be.${key}(value)`);
}

/**
 * @param {*} obj The JS object
 * @returns The size of a JS object
 */
Object.size = function (obj) {
  let size = 0,
    key;
  for (key in obj) {
    if (obj.hasOwnProperty(key)) size++;
  }
  return size;
};

/**
 * @param {*} obj The JS obj key
 * @param {*} arr The array to compare the key against
 * @returns True if the key is present
 */
function arrContains(obj, arr) {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i] === obj) {
      return true;
    }
  }
  return false;
}

/**
 * @param {*} el The returned element of a cy.get() method
 * @param {*} value The relative given object value
 * @returns An RGB value converted from a validated hexidecimal value
 * @returns A _'px'_ (pixel) value converted from a dynamic CSS length value
 */
 function _convertUnits(el, value) {
  var hexEval = /^#(?:[a-f\d]{3}){1,2}\b/i;
  if (value.slice(0, 1) === "#") {
    if (hexEval.test(value)) {
      return _hexToRgb(value);
    } else {
      console.error(`Invalid Hex value, the value: ${value} is invalid.`);
    }
  }

  for (let i = 0; i < _cssUnitsLength.length; i++) {
    let unitEval = /^[0-9]+(\.[0-9]+)?[a-zA-Z]+$/i;
    if (unitEval.test(value)) {
      if (value.includes(_cssUnitsLength[i])) {
        return units.toPx(el, value) + "px";
      } else {
        console.error(`Invalid Hex value, the value: ${value} is invalid.`);
      }
    }
  }
  return value;
}

/**
 * @note Stolen from https://stackoverflow.com/questions/5623838/rgb-to-hex-and-hex-to-rgb
 * @param {*} hex Either a shorthand or longhand Hex value
 * @returns The hex value converted to RGB format
 */
function _hexToRgb(hex) {
  // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
  var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
  hex = hex.replace(shorthandRegex, function (m, r, g, b) {
    return r + r + g + g + b + b;
  });

  var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

  if (result) {
    let r = parseInt(result[1], 16);
    let g = parseInt(result[2], 16);
    let b = parseInt(result[3], 16);

    return `rgb(${r}, ${g}, ${b})`;
  }
}

export {
  verifyElement,
};
